/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   node_push.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <kjaoudi@student.42nice.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/04 21:26:49 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/02/07 09:31:23 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "t_node.h"

#include <stddef.h>

void	node_push(t_node **head, t_node *new)
{
	t_node	*tmp;

	if (!*(head))
	{
		new->prev = 0;
		*head = new;
	}
	else
	{
		new->prev = 0;
		tmp = *head;
		tmp->prev = new;
		new->next = *head;
		*head = new;
	}
}
