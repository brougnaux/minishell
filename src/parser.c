/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <kjaoudi@student.42nice.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/03 07:33:49 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/03/08 20:13:08 by armansuy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include <stdio.h>

char	*expand_var(char *s);
char	*expand_bin(char *s);
char	*trim_spaces(char *s);
char	*trim_quotes(char *s);

static int	check_syntax(t_node *it, t_node **list)
{
	int	fail;

	fail = 0;
	if (it->prev && it->prev->data && *(char *) it->prev->data == 0)
		fail = 1;
	if (fail)
	{
		printf("syntax error near `%s'\n", (char *) it->data);
		g_shell.lastret = 258;
		node_clear(list);
	}
	return (!fail);
}

t_node	*parse(t_node **tok_list)
{
	t_node	*it;

	it = *tok_list;
	while (it)
	{
		if (*(char *) it->data)
		{
			if (!is_op(*(char *) it->data))
			{
				it->data = (void *) expand_var((char *) it->data);
				it->data = (void *) trim_quotes((char *) it->data);
				it->data = (void *) trim_spaces((char *) it->data);
				if (*tok_list == it || \
					(it->prev && is_op(*(char *) it->prev->data)))
					it->data = (void *) expand_bin((char *) it->data);
			}
			else if (!check_syntax(it, tok_list))
				return (0);
		}
		it = it->next;
	}
	return (*tok_list);
}
