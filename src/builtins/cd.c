/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <kjaoudi@student.42nice.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/03 07:34:44 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/02/25 08:54:05 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

static char	*check_input(char const *args)
{
	t_tuple const	*oldpwd = get_tuple(g_shell.env, "OLDPWD");
	t_tuple const	*localhome = get_tuple(g_shell.env, "HOME");
	char const		*syshome = getenv("HOME");

	if (args == NULL || !diff(args, "--"))
	{
		if (localhome)
			return ((char *) localhome->value);
		else if (syshome)
			return ((char *) syshome);
		else
			printf("cd: getenv() returned NULL\n");
	}
	else if (!diff(args, "-") && oldpwd)
		return (oldpwd->value);
	return ((char *) args);
}

static void	refresh_pwd(void)
{
	t_tuple const	*oldpwd = get_tuple(g_shell.env, "PWD");
	char const		*newpwd = getcwd(NULL, 0);

	if (oldpwd)
		set_tuple(&g_shell.env, "OLDPWD", oldpwd->value);
	if (newpwd)
	{
		set_tuple(&g_shell.env, "PWD", newpwd);
		free((char *) newpwd);
	}
	else
		printf("cd: getcwd() returned NULL\n");
}

int	cd(char const *args)
{
	int			ret;
	char const	*tmp = check_input(args);

	ret = 0;
	g_shell.lastret = EXIT_FAILURE;
	if (tmp != NULL)
	{
		if (chdir(tmp) != 0)
			printf("cd:  no such file or directory: %s\n", args);
		else
		{
			refresh_pwd();
			g_shell.lastret = EXIT_SUCCESS;
			ret = 1;
		}
	}
	return (ret);
}
