/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pwd.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/17 14:26:08 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/03/04 19:03:56 by armansuy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

#include <stdio.h>
#include <stdlib.h>

int	pwd(char const *args)
{
	t_tuple const	*tuple = get_tuple(g_shell.env, "PWD");

	(void)args;
	g_shell.lastret = EXIT_SUCCESS;
	if (tuple)
	{
		printf("%s\n", tuple->value);
		return (1);
	}
	return (0);
}
