/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   quit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <kjaoudi@student.42nice.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/03 07:59:15 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/03/08 16:08:53 by armansuy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

#include <stdio.h>
#include <stdlib.h>

static int	strisdigit(char const *s)
{
	if (*s && *s == '-')
		s++;
	while (*s)
		if (!ft_isdigit(*s++))
			return (0);
	return (1);
}

static void	check_digits(char **split)
{
	if (!strisdigit(*split))
	{
		printf("exit: %s: numeric argument required\n", *split);
		free_split(split);
		free_shell();
		exit(255);
	}
}

static int	check_args(char const *args, char **split)
{
	int	ret;

	ret = g_shell.lastret;
	if (*(split + 1))
	{
		free_split(split);
		printf("exit: too many arguments\n");
		ret = 1;
	}
	else
	{
		free_split(split);
		free_shell();
		exit(ft_atoi(args) % 256);
	}
	return (ret);
}

int	quit(char const *args)
{
	int		ret;
	char	**split;

	ret = g_shell.lastret;
	printf("exit\n");
	if (args)
	{
		split = ft_split(args, ' ');
		if (*split)
		{
			check_digits(split);
			ret = check_args(args, split);
			g_shell.lastret = ret;
		}
	}
	else
	{
		free_shell();
		exit(ret);
	}
	return (1);
}
