/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/17 14:26:05 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/02/22 17:25:22 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

#include <stdio.h>
#include <stdlib.h>

int	env(char const *args)
{
	t_node const	*node = g_shell.env;
	t_node			*tmp;
	t_tuple			*tuple;

	g_shell.lastret = EXIT_SUCCESS;
	if (!args || !(*args))
	{
		while (node)
		{
			tmp = node->next;
			tuple = (t_tuple *) node->data;
			if (tuple && tuple->name)
				printf("%s=%s\n", tuple->name, tuple->value);
			else
				node_del((t_node *) node);
			node = tmp;
		}
		return (1);
	}
	return (0);
}
