/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   export.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <kjaoudi@student.42nice.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/03 07:34:58 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/03/08 15:29:35 by armansuy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

#include <stdio.h>
#include <stdlib.h>

static void	detect_export_type(char const *var);
void		print_vars(char *found);
int			posix(char const *s);

static int	check_input(char const *input, char **pvars)
{
	char const	*varend = var_end(input);

	if (posix(input))
		return (1);
	else if (!ft_strchr(input, '=') && *varend == 0 && varend != input)
		return (1);
	else if (*varend == '=' && diff(input, "=") && varend != input)
		return (1);
	printf("export: `%s': not a valid identifier\n", input);
	free_split(pvars);
	g_shell.lastret = EXIT_FAILURE;
	return (0);
}

int	export(char const *args)
{
	char	**vars;
	char	**pvars;

	if (!args || !(*args))
		print_vars("");
	else
	{
		vars = ft_split(args, ' ');
		pvars = vars;
		while (vars && *vars)
		{
			if (!check_input(*vars, pvars))
				return (1);
			detect_export_type(*vars);
			vars++;
		}
		free_split(pvars);
	}
	g_shell.lastret = EXIT_SUCCESS;
	return (1);
}

static void	no_assign(char const *var)
{
	t_tuple	*find;

	find = get_tuple(g_shell.var_list, var);
	if (find && find->value)
	{
		set_tuple(&g_shell.env, find->name, find->value);
		free_tuple(find);
	}
	else
	{
		find = get_tuple(g_shell.env, var);
		if (!find)
			set_tuple(&g_shell.var_list, var, 0);
	}
}

static void	detect_export_type(char const *var)
{
	t_tuple	*find;
	char	*value;

	value = ft_strchr(var, '=');
	if (value)
	{
		*value++ = 0;
		set_tuple(&g_shell.env, var, value);
		find = get_tuple(g_shell.var_list, var);
		if (find)
			free_tuple(find);
	}
	else
		no_assign(var);
}
