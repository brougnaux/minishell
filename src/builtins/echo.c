/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   echo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <kjaoudi@student.42nice.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/03 07:34:48 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/03/03 14:00:55 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

#include <stdio.h>
#include <stdlib.h>

static char	*get_n_flag(char const *s)
{
	if (*s == '-' && (*(s + 1) == 'n'))
	{
		while (*(++s) == 'n')
			;
		if (*s == ' ' || *s == 0)
			return (strshift((char *) s, ' '));
		else
			return (0);
	}
	return (0);
}

static char	*get_flags(char const *s)
{
	s = get_n_flag(s);
	if (s)
	{
		while (get_n_flag(s))
			s = get_n_flag(s);
		return ((char *) s);
	}
	return (0);
}

int	echo(char const *args)
{
	char	*flagged;

	if (args)
	{
		flagged = get_flags(args);
		if (flagged)
			printf("%s", flagged);
		else
			printf("%s\n", args);
	}
	else
		printf("\n");
	g_shell.lastret = EXIT_SUCCESS;
	return (1);
}
