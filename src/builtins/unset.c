/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   unset.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <kjaoudi@student.42nice.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/03 07:35:25 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/03/04 16:55:28 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

#include <stdio.h>
#include <stdlib.h>

int	posix(char const *s);

static int	check_input(char const *input, char **vars)
{
	if (ft_isdigit(*input) || !posix(input))
	{
		printf("unset: `%s': not a valid identifier\n", input);
		g_shell.lastret = EXIT_FAILURE;
		free_split(vars);
		return (0);
	}
	return (1);
}

int	unset(char const *args)
{
	t_tuple	*find;
	char	**vars;
	char	**tmp;

	if (args)
	{
		tmp = ft_split(args, ' ');
		vars = tmp;
		while (vars && *vars)
		{
			if (!check_input(*vars, tmp))
				return (1);
			find = get_tuple(g_shell.env, *vars);
			if (!find)
				find = get_tuple(g_shell.var_list, *vars);
			if (find)
				free_tuple(find);
			vars++;
		}
		free_split(tmp);
	}
	g_shell.lastret = EXIT_SUCCESS;
	return (1);
}
