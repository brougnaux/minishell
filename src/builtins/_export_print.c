/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   _export_print.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: armansuy <armansuy@student.42nice.f>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/08 15:24:08 by armansuy          #+#    #+#             */
/*   Updated: 2022/03/08 15:25:31 by armansuy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

#include <stdio.h>

static t_tuple	*find_print(t_tuple *find, char *found, t_node *env)
{
	t_tuple	*data;

	while (env)
	{
		data = env->data;
		if (data->name)
			if (ft_strcmp(found, data->name) < 0)
				if (!find || ft_strcmp(find->name, data->name) > 0)
					find = data;
		env = env->next;
	}
	return (find);
}

void	print_vars(char *found)
{
	t_tuple	*find;

	find = 0;
	find = find_print(find, found, g_shell.env);
	find = find_print(find, found, g_shell.var_list);
	if (find && find->name)
	{
		if (find->value)
			printf("declare -x %s=\"%s\"\n", find->name, find->value);
		else
			printf("declare -x %s\n", find->name);
		print_vars(find->name);
	}
}
