/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_tuples.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/09 09:57:58 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/02/25 10:05:51 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

#include <stdio.h>

void	print_tuples(t_node *tuple_list)
{
	t_tuple	*t;
	t_node	*it;

	t = 0;
	it = tuple_list;
	while (it)
	{
		t = (t_tuple *) it->data;
		printf("[%s][%s]\n", t->name, t->value);
		it = it->next;
	}
}
