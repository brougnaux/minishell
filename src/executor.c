/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   executor.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/08 09:10:07 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/03/03 16:11:13 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

#include <stdio.h>

t_node const	*pipes(t_node *node);
t_node const	*append(t_node *node);
t_node const	*overwrite(t_node *node);
t_node const	*heredoc(t_node *node);
t_node			*default_launch(t_node *node);
t_node			*find_redirection(t_node *node, t_redir *redir_type);

static int	unknown_operator(t_node *node)
{
	char const	*input = node->data;

	if (is_op(*input))
		return (1);
	return (0);
}

static int	blank_data(t_node *node)
{
	char const	*data = node->data;

	if (data == 0 || *data == 0)
		return (1);
	return (0);
}

t_node	*redirect_launch(t_node *node, t_redir redir_type)
{
	int				i;
	int const		redirs[] = {PIPE, APPEND, OVERWRITE, HEREDOC};
	t_node const	*(*f[])(t_node *) = \
		{pipes, append, overwrite, heredoc, 0};

	i = -1;
	while (f[++i])
		if (redirs[i] == (int) redir_type)
			return ((t_node *) f[i](node));
	return (0);
}

int	execute(void)
{
	t_node	*node;
	t_redir	redir_type;

	node = g_shell.cmd_list;
	while (node)
	{
		if (blank_data(node))
			node = node->next;
		else if (assign_var(node->data))
			node = node->next;
		else if (unknown_operator(node))
			node = 0;
		else if (find_redirection(node->next, &redir_type))
			node = redirect_launch(node, redir_type);
		else
			node = default_launch(node);
	}
	return (1);
}
