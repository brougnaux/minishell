/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   _wait_signal.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: armansuy <armansuy@student.42nice.f>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/08 15:11:28 by armansuy          #+#    #+#             */
/*   Updated: 2022/03/08 16:38:12 by armansuy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include <sys/wait.h>

#include <stdio.h>

void	wait_signal(void)
{
	int	status;

	waitpid(g_shell.lastpid, &status, 0);
	while (waitpid(-1, &status, 0) > 0)
	{
		if (WIFEXITED(status))
			g_shell.lastret = WEXITSTATUS(status);
		else if (WIFSIGNALED(status))
		{
			if (WTERMSIG(status) == 2)
				g_shell.lastret = 130;
			else if (WTERMSIG(status) == 3)
				g_shell.lastret = 131;
		}
	}
	if (status == 32512)
		g_shell.lastret = 127;
	else if (status == 512)
		g_shell.lastret = 2;
}
