/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   _pipes.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/21 15:51:13 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/03/04 18:41:14 by armansuy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

void	launch_bin(char **cmd);
int		launch_builtin(char **cmd);
t_node	*find_redirection(t_node *node, t_redir *redir_type);
t_node	*get_format(t_node *node, char ***cmdptr);
void	wait_signal(void);

static int	count_pipes(t_node *node)
{
	t_redir	redir_type;
	t_node	*tmp;
	int		count;

	count = 0;
	tmp = node;
	while (tmp)
	{
		tmp = find_redirection(tmp->next, &redir_type);
		if (tmp && redir_type == PIPE)
			count++;
	}
	return (count);
}

static void	close_2(int *oldfd, int *newfd, int norm)
{
	if (newfd)
	{
		close(newfd[0]);
		close(newfd[1]);
	}
	if (oldfd)
	{
		close(oldfd[0]);
		close(oldfd[1]);
	}
	if (norm)
		free(oldfd);
}

static int	dup_pos(int pos, int pipes_count, int *oldfd, int *newfd)
{
	if (pos != 0)
		if (dup2(oldfd[0], STDIN_FILENO) == -1)
			return (0);
	if (pos != pipes_count)
		if (dup2(newfd[1], STDOUT_FILENO) == -1)
			return (0);
	return (1);
}

static int	*pipe_launch(char **cmd, int pos, int pipes_count, int *oldfd)
{
	pid_t	cpid;
	int		*newfd;

	newfd = malloc(sizeof(int) * 2);
	if (!newfd || pipe(newfd) == -1)
		return (0);
	cpid = fork();
	g_shell.lastpid = cpid;
	if (cpid == 0)
	{
		g_shell.lastpid = getpid();
		dup_pos(pos, pipes_count, oldfd, newfd);
		close_2(oldfd, newfd, 0);
		if (launch_builtin(cmd))
			exit(EXIT_SUCCESS);
		else
			launch_bin(cmd);
	}
	close_2(oldfd, 0, 0);
	free(oldfd);
	free_split(cmd);
	return (newfd);
}

t_node const	*pipes(t_node *node)
{
	int		pos;
	int		*oldfd;
	char	**cmd;
	int		pipes_count;
	t_node	*ret;

	oldfd = malloc(sizeof(int) * 2);
	if (!oldfd || pipe(oldfd) == -1)
		return (0);
	pos = 0;
	ret = get_format(node, &cmd);
	pipes_count = count_pipes(node);
	while (pos <= pipes_count)
	{
		oldfd = pipe_launch(cmd, pos, pipes_count, oldfd);
		if (pos++ < pipes_count)
			ret = get_format(ret->next, &cmd);
	}
	wait_signal();
	close_2(oldfd, 0, 1);
	return (ret);
}
