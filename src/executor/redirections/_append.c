/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   _append.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/21 15:50:12 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/02/21 20:25:27 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

	//int const	fd = open(file, O_RDWR | O_CREAT | O_APPEND, 0644);
t_node const	*append(t_node *node)
{
	return (node->next);
}
