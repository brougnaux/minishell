/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   _find_redirection.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <kjaoudi@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/18 18:17:01 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/02/22 11:02:59 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static t_node	*find_redir_range(t_node *node, t_redir *redir_type, int range)
{
	t_redir const	ret[] = {PIPE, APPEND, OVERWRITE, HEREDOC};
	char const		*ops[] = {"|", ">>", ">", "<<", 0};
	char			*data;
	int				i;

	if (node && node->data && (range > 0))
	{
		i = -1;
		data = node->data;
		while (ops[++i])
		{
			if (!diff(data, ops[i]))
			{
				*redir_type = ret[i];
				return (node->next);
			}
		}
		return (find_redir_range(node->next, redir_type, range - 1));
	}
	return (0);
}

t_node	*find_redirection(t_node *node, t_redir *redir_type)
{
	return (find_redir_range(node, redir_type, 2));
}
