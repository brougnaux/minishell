/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   _overwrite.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/21 16:05:45 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/02/21 20:25:33 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

	//fd = open(file, O_RDWR | O_CREAT | O_overwrite, 0644);
t_node const	*overwrite(t_node *node)
{
	return (node->next);
}
