/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   _launcher.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/08 15:56:53 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/03/08 16:26:38 by armansuy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

t_node	*get_format(t_node *node, char ***cmdptr);
void	wait_signal(void);

int	launch_builtin(char **cmd)
{
	void	(*f)(char const *);

	f = get_builtin(cmd[0]);
	if (f)
	{
		(*f)(cmd[1]);
		return (1);
	}
	return (0);
}

void	launch_bin(char **cmd)
{
	int	value;

	if (execve(cmd[0], cmd, g_shell.default_env) == -1)
	{
		if (cmd[0] == 0)
		{
			printf("syntax error\n");
			value = 1;
		}
		else
		{
			printf("%s: %s\n", cmd[0], "command not found");
			value = 127;
		}
		free_shell();
		exit(value);
	}
}

void	subprocess(char **cmd)
{
	pid_t const	cpid = fork();

	g_shell.lastpid = cpid;
	if (cpid == -1)
	{
		printf("launch_bin: fork failed\n");
		return ;
	}
	if (cpid == 0)
		launch_bin(cmd);
	wait_signal();
}

t_node	*default_launch(t_node *node)
{
	char	**cmd;
	t_node	*next_launch;

	cmd = NULL;
	next_launch = get_format(node, &cmd);
	if (!launch_builtin(cmd))
		subprocess(cmd);
	free_split(cmd);
	return (next_launch);
}
