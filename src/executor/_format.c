/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   _format.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <kjaoudi@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/18 11:44:35 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/02/25 09:32:45 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

#include <stdlib.h>

static char	**get_cmd(char *cmd, char *args)
{
	char	**ret;

	if (get_builtin(cmd))
	{
		ret = ft_calloc(3, sizeof(char *));
		ret[0] = ft_strdup(cmd);
		ret[1] = args;
	}
	else
	{
		cmd = ft_strjoin(cmd, " ");
		if (args)
		{
			cmd = my_strjoin(cmd, args);
			free(args);
		}
		ret = ft_split(cmd, ' ');
		free(cmd);
	}
	return (ret);
}

char	**format_input(t_node *node, t_node *next)
{
	if (node && node->data)
	{
		if (next && next->data)
			return (get_cmd(node->data, ft_strdup(next->data)));
		else
			return (get_cmd(node->data, NULL));
	}
	return (NULL);
}

t_node	*get_format(t_node *node, char ***cmdptr)
{
	t_node	*next;

	next = node->next;
	if (next && next->data && !is_op(*(char *) next->data))
	{
		*cmdptr = format_input(node, next);
		next = next->next;
	}
	else
		*cmdptr = format_input(node, 0);
	return (next);
}
