/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tokenizer.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <kjaoudi@student.42nice.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/03 07:34:17 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/03/04 16:06:29 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

#include <stdio.h>
#include <stdlib.h>

char	*trim_spaces(char *s);

static char	*split(t_node **list, char **str, char *start, char *operator)
{
	node_add(list, node_new((void *) ft_strndup(start, (*str - start))));
	if (operator)
	{
		if (**str != '|' || is_op(*(++(*str))) || is_op(*(operator + 1)))
		{
			printf("syntax error near `%c'\n", **str);
			node_clear(list);
			g_shell.lastret = 258;
			return (0);
		}
		node_add(list, node_new((void *) \
			ft_strndup(operator, (*str - operator))));
	}
	return (*str);
}

static char	*skip_quotes(char *cursor)
{
	char	*ret;

	ret = cursor;
	if (*cursor == '\'' || *cursor == '"')
	{
		ret = ft_strchr((cursor + 1), *cursor);
		if (!ret)
		{
			printf("syntax error near `%c'\n", *cursor);
			g_shell.lastret = 258;
			return (NULL);
		}
	}
	return (ret);
}

static t_node	*remainer(char const *start, t_node **list, char *str)
{
	if (start)
	{
		node_add(list, node_new((void *) ft_strndup(start, str - start)));
		return (parse(list));
	}
	return (0);
}

t_node	*tokenize(char *str)
{
	char const	*start = trim_spaces(str);
	char		*cursor;
	int			space;
	t_node		*list;

	list = 0;
	space = 1;
	while (*str && start)
	{
		cursor = str;
		str = skip_quotes(cursor);
		if (!str)
			return (0);
		if (is_op(*cursor))
		{
			space = 1;
			start = split(&list, &str, (char *) start, cursor);
		}
		else if (*cursor == ' ' && space && space--)
			start = split(&list, &str, (char *) start, 0);
		else
			str++;
	}
	return (remainer(start, &list, str));
}
