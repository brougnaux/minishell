/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   _expand_bin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/08 09:28:06 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/02/22 17:27:21 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

#include <stdlib.h>

char	*expand_bin(char *cmd)
{
	char	*find;

	if (!get_builtin(cmd))
	{
		find = get_binpath(cmd);
		if (find)
		{
			free(cmd);
			return ((char *) find);
		}
	}
	return (cmd);
}
