/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   _expand_var.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <kjaoudi@student.42nice.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/03 07:33:39 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/03/04 15:12:08 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

static char	*value(char **ret, char **cursor, char **cmd, int value)
{
	char	*nb;

	nb = ft_itoa(value);
	*ret = my_strjoin(*ret, *cursor);
	*ret = my_strjoin(*ret, nb);
	free(nb);
	*cursor = ++(*cmd);
	return (*cursor);
}

static char	*join(char **ret, char **cursor, char **cmd)
{
	t_tuple	*tuple;
	char	*find;

	*(*cmd)++ = 0;
	if (**cmd == '?')
		return (value(ret, cursor, cmd, g_shell.lastret));
	else if (**cmd == '$')
		return (value(ret, cursor, cmd, getpid()));
	find = ft_strndup(*cmd, (var_end(*cmd) - *cmd));
	tuple = get_tuple((t_node *) g_shell.env, find);
	if (tuple)
		*ret = my_strjoin(my_strjoin(*ret, *cursor), tuple->value);
	else
	{
		tuple = get_tuple((t_node *) g_shell.var_list, find);
		if (tuple)
			*ret = my_strjoin(my_strjoin(*ret, *cursor), tuple->value);
		else
			*ret = my_strjoin(*ret, *cursor);
	}
	free(find);
	*cursor = var_end(*cmd);
	return (*cursor);
}

static char	*find(char **ret, char **cursor, char **cmd)
{
	if (**cmd == '$' && *(*cmd + 1) != ' ' && *(*cmd + 1))
		*cmd = join(ret, cursor, cmd);
	else
		(*cmd)++;
	return (*cmd);
}

char	*expand_var(char *cmd)
{
	char const	*freecmd = cmd;
	char const	*cursor = cmd;
	char		*stop;
	char		*ret;

	ret = ft_strdup("");
	while (*cmd)
	{
		if (*cmd == '"' || *cmd == '$')
		{
			stop = 1 + ft_strchr((cmd + 1), '"');
			if (*cmd == '$')
				stop = var_end(cmd + 1);
			while (cmd < stop)
				cmd = find(&ret, (char **) &cursor, &cmd);
		}
		else if (*cmd == '\'')
			cmd = 1 + ft_strchr((cmd + 1), '\'');
		else
			cmd++;
	}
	ret = my_strjoin(ret, cursor);
	free((char *) freecmd);
	return (ret);
}
