/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_envp.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <kjaoudi@student.42nice.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/03 07:33:45 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/03/04 16:50:47 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

#include <stdio.h>
#include <stdlib.h>

static int	unknown_path(t_node *env)
{
	t_tuple const	*path = get_tuple(env, "PATH");

	return (0);
	if (!path)
		return (1);
	return (0);
}

int	get_envp(char **envp)
{
	char	*name;
	char	*value;

	if (envp)
	{
		g_shell.default_env = envp;
		while (*envp)
		{
			name = ft_strndup(*envp, ft_strchr(*envp, '=') - *envp);
			value = 1 + ft_strchr(*envp, '=');
			set_tuple(&g_shell.env, name, value);
			free(name);
			envp++;
		}
		if (unknown_path(g_shell.env))
		{
			printf("get_envp: PATH not set\n");
			return (0);
		}
		return (1);
	}
	return (0);
}
