/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_tuple.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <kjaoudi@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/11 15:56:36 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/03/04 17:02:26 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils.h"

#include <stdlib.h>

int	set_tuple(t_node **list, char const *find, char const *newval)
{
	t_tuple			*tuple;

	tuple = get_tuple(*list, find);
	if (tuple)
	{
		if (tuple->value)
		{
			free(tuple->value);
			tuple->value = 0;
		}
		if (newval)
			tuple->value = ft_strdup(newval);
		else
			tuple->value = NULL;
		return (1);
	}
	return (add_tuple(list, find, newval));
}
