/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_tuple.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <kjaoudi@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/11 15:56:45 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/03/04 15:58:10 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "t_node.h"
#include "nodes.h"
#include "types.h"
#include "libft.h"

int	add_tuple(t_node **list, char const *newvar, char const *newval)
{
	t_tuple	*tuple;

	tuple = ft_calloc(1, sizeof(*tuple));
	if (tuple)
	{
		tuple->name = ft_strdup(newvar);
		if (newval)
			tuple->value = ft_strdup(newval);
		else
			tuple->value = NULL;
		node_add(list, node_new((void *) tuple));
		return (1);
	}
	return (0);
}
