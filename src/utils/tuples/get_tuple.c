/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_tuple.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <kjaoudi@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/11 15:56:18 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/02/22 17:19:44 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils.h"

t_tuple	*get_tuple(t_node *list, char const *find)
{
	t_tuple	*tuple;

	while (list)
	{
		tuple = (t_tuple *) list->data;
		if (!diff(tuple->name, find))
			return (tuple);
		list = list->next;
	}
	return (0);
}
