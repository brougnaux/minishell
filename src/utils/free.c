/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <kjaoudi@student.42nice.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/03 07:31:59 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/03/04 15:56:27 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

#include <stdlib.h>

void	free_tuple(t_tuple *tuple)
{
	if (tuple->name)
	{
		free(tuple->name);
		tuple->name = 0;
	}
	if (tuple->value)
	{
		free(tuple->value);
		tuple->value = 0;
	}
}

void	free_list(t_node **list)
{
	if (*list)
		node_apply(*list, &free_tuple);
	node_clear(list);
}

void	free_shell(void)
{
	if (g_shell.input)
	{
		free(g_shell.input);
		g_shell.input = 0;
	}
	free_list(&g_shell.env);
	free_list(&g_shell.var_list);
	node_clear(&g_shell.cmd_list);
}
