/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   var_end.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <kjaoudi@student.42nice.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/03 07:32:57 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/03/04 15:39:10 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ctype.h"

static int	is_valid(char c)
{
	return (ft_isalpha(c) || ft_isdigit(c) || c == '_');
}

int	posix(char const *s)
{
	if (!(ft_isalpha(*s) || *s == '_'))
		return (0);
	while (*s)
		if (!is_valid(*s++))
			return (0);
	return (1);
}

char	*var_end(char const *s)
{
	if (is_valid(*s) && !ft_isdigit(*s))
		while (*s && is_valid(*s))
			s++;
	return ((char *) s);
}
