/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_delim.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/18 10:31:36 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/02/22 17:28:27 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

#include <stdio.h>
#include <stdlib.h>
#include <readline/readline.h>

char	*expand_var(char *cmd);

char	*get_delim(char *delim)
{
	char		*ret;
	char		*line;

	ret = ft_strdup("");
	line = readline("> ");
	while (line)
	{
		if (diff(line, delim) == 0)
		{
			free(line);
			break ;
		}
		line = my_strjoin(line, "\n");
		ret = my_strjoin(ret, line);
		free(line);
		line = readline("> ");
		if (!line)
			printf("warning : EOL detected\n");
	}
	return (expand_var(ret));
}
