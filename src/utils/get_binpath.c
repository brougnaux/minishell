/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_binpath.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <kjaoudi@student.42nice.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/06 16:47:18 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/03/05 12:30:12 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

#include <sys/stat.h>
#include <sys/types.h>

static int	check_permissions(char const *filepath)
{
	struct stat	filestat;

	if (stat(filepath, &filestat) == 0)
	{
		if ((filestat.st_mode & S_IXUSR)
			&& (filestat.st_mode & S_IXGRP)
			&& (filestat.st_mode & S_IXOTH))
		{
			return (1);
		}
	}
	return (0);
}

char	*get_binpath(char const *find)
{
	char			**split;
	char			**tmp;
	char			*ret;
	t_tuple const	*path = get_tuple(g_shell.env, "PATH");

	if (check_permissions(find))
		return (ft_strdup(find));
	ret = NULL;
	if (!path || find[0] == '/')
		return (ret);
	split = ft_split((char *)path->value, ':');
	tmp = split;
	while (tmp && *tmp)
	{
		*tmp = my_strjoin(*tmp, "/");
		*tmp = my_strjoin(*tmp, find);
		if (check_permissions(*tmp))
		{
			ret = ft_strdup(*tmp);
			break ;
		}
		tmp++;
	}
	free_split(split);
	return (ret);
}
