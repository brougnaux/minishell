/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   assign_var.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <kjaoudi@student.42nice.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/13 15:55:04 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/03/04 15:29:26 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	assign_var(char const *str)
{
	char		*value;

	value = var_end(str);
	if (*value == '=' && value != str)
	{
		*value++ = 0;
		if (get_tuple(g_shell.env, str))
			set_tuple(&g_shell.env, str, value);
		else
			set_tuple(&g_shell.var_list, str, value);
		return (1);
	}
	return (0);
}
