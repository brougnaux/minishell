/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_builtin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <kjaoudi@student.42nice.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/08 17:38:57 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/02/09 10:40:21 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	*get_builtin(char const *cmd)
{
	int			i;
	char const	*names[] = {"exit", "echo", "pwd", "cd", "env", \
		"export", "unset", 0};
	void const	*funcs[] = {&quit, &echo, &pwd, &cd, &env, \
		&export, &unset, 0};

	i = -1;
	while (names[++i])
		if (!diff(cmd, names[i]))
			return ((int *) funcs[i]);
	return (0);
}
