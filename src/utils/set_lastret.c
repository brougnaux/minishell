/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_lastret.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/28 14:01:12 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/03/03 15:34:28 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	set_lastret(int const errno)
{
	int const	errs[] = {0, 2, 13, 10, -1};
	int const	vals[] = {0, 127, 127, 1};
	int			i;

	i = -1;
	while (errs[++i] != -1)
		if (errno == errs[i])
			g_shell.lastret = vals[i];
}
