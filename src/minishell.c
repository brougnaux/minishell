/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <kjaoudi@student.42nice.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/04 18:47:41 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/03/08 16:32:41 by armansuy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <termios.h>
#include <readline/readline.h>
#include <readline/history.h>

static int	show_ctlr(int b)
{
	struct termios	new;

	tcgetattr(0, &new);
	if (b)
		new.c_lflag |= ECHOCTL;
	else
		new.c_lflag &= ~ECHOCTL;
	tcsetattr(0, TCSANOW, &new);
	return (1);
}

static void	sighandler(int signum)
{
	if (signum == SIGINT)
		printf("\n");
	if (g_shell.lastpid)
	{
		if (signum == SIGINT && kill(g_shell.lastpid, signum))
			wait(0);
		if (signum == SIGQUIT)
		{
			if (kill(g_shell.lastpid, signum))
				wait(0);
			else
				printf("Quit: 3\n");
		}
		g_shell.lastpid = 0;
	}
	else
	{
		rl_on_new_line();
		if (signum == SIGINT)
			rl_replace_line("", 0);
		rl_redisplay();
	}
}

static void	init_sig(struct sigaction *s_act)
{
	show_ctlr(0);
	s_act->sa_handler = sighandler;
	s_act->sa_flags = SA_SIGINFO;
	sigaction(SIGINT, s_act, NULL);
	sigaction(SIGQUIT, s_act, NULL);
}

int	main(int ac, char **av, char **envp)
{
	struct sigaction	s_act;

	g_shell = (struct s_shell){0};
	init_sig(&s_act);
	if (ac == 1 && av[1] == 0 && get_envp(envp))
	{
		g_shell.input = readline("minishell $ ");
		while (g_shell.input)
		{
			if (*g_shell.input)
			{
				add_history(g_shell.input);
				g_shell.cmd_list = tokenize(g_shell.input);
				if (g_shell.cmd_list && show_ctlr(1))
					execute();
				node_clear(&g_shell.cmd_list);
			}
			free(g_shell.input);
			g_shell.lastpid = 0;
			show_ctlr(0);
			g_shell.input = readline("minishell $ ");
		}
		quit(0);
	}
	return (EXIT_FAILURE);
}
