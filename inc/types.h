/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   types.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <kjaoudi@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/22 17:17:05 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/02/22 17:17:17 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TYPES_H
# define TYPES_H

typedef enum s_redir
{
	PIPE = 1,
	APPEND,
	OVERWRITE,
	HEREDOC
}	t_redir;

typedef struct s_tuple
{
	char	*name;
	char	*value;
}	t_tuple;

#endif
