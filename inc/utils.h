/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <kjaoudi@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/21 19:58:40 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/02/28 14:01:04 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UTILS_H
# define UTILS_H

# include "t_node.h"
# include "nodes.h"
# include "libft.h"
# include "types.h"

t_tuple	*get_tuple(t_node *list, char const *find);
int		set_tuple(t_node **list, char const *find, char const *newval);
int		add_tuple(t_node **list, char const *newvar, char const *newval);

char	*get_delim(char *delim);
char	*get_binpath(char const *find);
void	*get_builtin(char const *cmd);
void	set_lastret(int const errno);
int		get_envp(char **envp);
int		assign_var(char const *str);

int		is_op(char c);
int		diff(char const *cmp, char const *ref);
char	*var_end(char const *s);
char	*strshift(char *str, char c);
char	*my_strjoin(char const *s1, char const *s2);

void	free_shell(void);
void	free_list(t_node **list);
void	free_tuple(t_tuple *tuple);

#endif
