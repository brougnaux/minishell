/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaoudi <kjaoudi@student.42nice.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/19 16:38:55 by kjaoudi           #+#    #+#             */
/*   Updated: 2022/03/04 13:59:34 by kjaoudi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# include "utils.h"
# include "tmp.h"

# include <sys/types.h>

struct s_shell
{
	pid_t	lastpid;
	char	**default_env;
	char	*input;
	int		lastret;
	t_node	*env;
	t_node	*cmd_list;
	t_node	*var_list;
}	g_shell;

t_node	*tokenize(char *input);
t_node	*parse(t_node **tok_list);

int		execute(void);

int		quit(char const *args);
int		echo(char const *args);
int		pwd(char const *args);
int		cd(char const *args);
int		env(char const *args);
int		export(char const *args);
int		unset(char const *args);

#endif
